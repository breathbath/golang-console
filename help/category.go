package help

type Category struct {
	Title    string
	IsChild bool
	HelpRows []Row
}

func (c *Category) AddHelpRow (row Row) {
	c.HelpRows = append(c.HelpRows, row)
}