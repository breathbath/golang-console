package help

import (
	"bitbucket.org/breathbath/go-console/command"
	"bitbucket.org/breathbath/go-console/consoleio"
	"fmt"
	"strings"
)

const HELP_TEXT = `
./console [-h|-v|-n] commandName [paramName=paramValue]

./console -h //shows list of commands and commands format
./console -h commandName //shows help specific to command
./console -v|-n commandName //increases|decreases the verbosity of the command output
./console commandName param1=paramValue1 //sends param1 with value paramValue1 to command
`

func CreateGeneralHelpContainer(locator command.CommandLocator) Container {
	helpContainer := NewHelpContainer("Console version 1.0")
	helpContainer.AddNewHelpString("Usage", "", HELP_TEXT, false)

	options := consoleio.NewGeneralOptions()
	for _, option := range options {
		helpContainer.AddNewHelpString("Options", "-" + option.Name , option.Description, false)
	}

	helpContainer.AddNewHelpCategory("Available commands", false)

	for _, command := range locator.Commands {
		helpContainer.AddNewHelpString(command.GetPackageName(), command.GetName(), command.GetDescription(), true)
	}

	return *helpContainer
}

func CreateHelpContainer(currentCommand command.Command) Container {
	helpContainer := NewHelpContainer("Help for command \"" + currentCommand.GetName() + "\"")
	helpContainer.AddNewHelpString("Description", "", currentCommand.GetDescription(), false)

	paramsList := ""
	paramDescriptions := map[string]string{}
	var toRequireString, defaultValue, description string

	paramCommand, ok := currentCommand.(command.CommandWithParams)
	fmt.Println(paramCommand.GetParams())

	options := consoleio.NewGeneralOptions()
	if (ok) {
		options = append(options, paramCommand.GetOptions()...)
		for _, param := range paramCommand.GetParams() {
			if param.IsRequired {
				toRequireString = "*"
			} else {
				toRequireString = ""
			}

			if param.DefaultValue != "" {
				defaultValue = "(default '" + param.DefaultValue + "')"
			} else {
				defaultValue = ""
			}

			paramsList = paramsList + " " + param.ParamName + "=<value>" + defaultValue

			if toRequireString != "" {
				toRequireString = "(*is required)"
			}

			description = param.Description + defaultValue
			paramDescriptions[param.ParamName + toRequireString] = description
		}
	}

	paramsList = strings.Trim(paramsList, " ")

	optionNames := options.GetOptionsNames()

	command := fmt.Sprintf("./console [%s] %s %s", strings.Join(optionNames, "|"), currentCommand.GetName(), paramsList)
	helpContainer.AddNewHelpString("Usage", "", command, false)

	for _, option := range options {
		helpContainer.AddNewHelpString("Params", "-" + option.Name , option.Description, false)
	}

	for paramName, descr := range paramDescriptions {
		helpContainer.AddNewHelpString("Params", paramName, descr, false)
	}

	helpContainer.AddNewHelpString("Note", "", "Params marked with * are required", false)

	return *helpContainer
}