package help

import (
	"fmt"
	"github.com/fatih/color"
)

type HelpResponseCreator struct {}

const SUB_CAT_SHIFT = " "
const ITEM_TITLE_SHIFT = "   "
const ITEM_DESCR_SHIFT = "  "

func (this *HelpResponseCreator) CreateHelpResponse(helpContainer Container) {
	color.Magenta(helpContainer.Title)
	for _, cat := range helpContainer.Categories {
		if(cat.IsChild) {
			color.Yellow(SUB_CAT_SHIFT + cat.Title)
		} else {
			fmt.Println("")
			color.Yellow(cat.Title + ":")
		}
		green := color.New(color.FgGreen).SprintFunc()
		black := color.New(color.FgBlue).SprintFunc()
		for _, item := range cat.HelpRows {
			shiftLeft := ITEM_TITLE_SHIFT
			shiftMiddle := ITEM_DESCR_SHIFT
			if(item.Title == "") {
				shiftLeft = ""
			}
			if(item.Description == "") {
				shiftMiddle = ""
			}
			fmt.Printf(shiftLeft + "%s" + shiftMiddle + "%s\n", green(item.Title), black(item.Description))
		}
	}
}