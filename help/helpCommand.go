package help

import (
	"bitbucket.org/breathbath/go-console/command"
	"bitbucket.org/breathbath/go-console/consoleio"
	"errors"
)

const HELP_NAME  = "help"

type HelpShower struct{
	command.ParametrisedCommand
	locator *command.CommandLocator
}

func NewHelpCommand(locator *command.CommandLocator) (command.Command) {
	return &HelpShower{locator: locator}
}

func (this HelpShower) Execute(request *consoleio.Request, response *consoleio.Response) {
	var helpData Container
	if request.CommandNameEmpty() {
		helpData = CreateGeneralHelpContainer(*this.locator)
		response.AddError(errors.New("No command name provided"))
	} else {
		comm, ok := this.locator.GetCommandByName(request.CommandName)
		if !ok {
			response.AddErrorMessage("Cannot show help for an unknown command " + request.CommandName)
			return
		}
		helpData = CreateHelpContainer(comm)
	}

	helpResponseCreator := &HelpResponseCreator{}
	helpResponseCreator.CreateHelpResponse(helpData)
}

func (pwd HelpShower) GetPackageName() string {
	return "general"
}

func (pwd HelpShower) GetName() string {
	return HELP_NAME
}

func (pwd HelpShower) GetDescription() string {
	return "Shows the general help information"
}