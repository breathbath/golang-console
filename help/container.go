package help

type Container struct {
	Categories [] Category
	Title string
	SearchMap map[string] int
}

func NewHelpContainer(title string) *Container {
	return &Container{Categories:[]Category{}, Title:title, SearchMap:make(map[string]int)}
}

func (this *Container) AddNewHelpCategory(catName string, isChild bool) {
	_, ok := this.SearchMap[catName]
	if ok {
		return
	}
	cat := Category{Title: catName, IsChild:isChild}
	this.Categories = append(this.Categories, cat)
	this.SearchMap[catName] = len(this.Categories) - 1
}

func (this *Container) AddNewHelpString(catName, title, desc string, isChild bool) {
	var cat Category
	hr := Row{Title: title, Description: desc }

	catPos, ok := this.SearchMap[catName]
	if ok {
		cat = this.Categories[catPos]
		cat.AddHelpRow(hr)
		this.Categories[catPos] = cat
	} else{
		cat = Category{Title: catName, IsChild:isChild, HelpRows:[]Row{hr}}
		this.Categories = append(this.Categories, cat)
		this.SearchMap[catName] = len(this.Categories) - 1
	}
}