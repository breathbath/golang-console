package main

import (
	"bitbucket.org/breathbath/go-console/executor"
	"os"
	"log"
	"fmt"
	"bitbucket.org/breathbath/go-console/commands"
)

func main() {
	context := executor.Context{
		Arguments:os.Args,
		CommandLocator: commands.BuildCommandsLocator(),
		LogMethod: func(errors [] error) {
			log.Fatal(errors)
		},
		OutputMethod: func(message string) {
			fmt.Println(message)
		},
	}
	executor.Execute(context)
}