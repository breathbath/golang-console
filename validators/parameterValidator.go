package validators

import "bitbucket.org/breathbath/go-console/consoleio"

type ParameterValidator interface {
	Validate(value string, parameter consoleio.Parameter, request consoleio.Request) error
}