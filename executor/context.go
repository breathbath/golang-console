package executor

import "bitbucket.org/breathbath/go-console/command"

type Context struct{
	Arguments      []string
	CommandLocator *command.CommandLocator
	LogMethod      func(errors [] error)
	OutputMethod   func(message string)
}