package executor

import (
	"bitbucket.org/breathbath/go-console/consoleio"
	"bitbucket.org/breathbath/go-console/command"
	"errors"
	"bitbucket.org/breathbath/go-console/validators"
	"bitbucket.org/breathbath/go-console/help"
)

func createIo(arguments []string) (*consoleio.Request, *consoleio.Response) {
	request := consoleio.NewRequestFromArgs(arguments)
	response := consoleio.NewResponse()

	optionsApplier := consoleio.NewOptionsApplier()
	optionsApplier.Apply(request, response)

	return request, response
}

func validateRequestInCommand(request *consoleio.Request, response *consoleio.Response, curCommand command.Command) error {
	if curCommand == nil {
		return errors.New("Unknown command " + request.CommandName)
	}

	if paramCommand, ok := curCommand.(command.CommandWithParams); ok {
		requestValidator := validators.NewRequestValidator(paramCommand.GetOptions())
		err := requestValidator.ValidateRequest(request)
		if err != nil {
			response.AddError(err)
			return err
		}
	}

	err := curCommand.Validate(request)
	if err != nil {
		response.AddError(err)
	}

	return err
}

func addHelpCommand(locator *command.CommandLocator) {
	locator.AddCommand(help.NewHelpCommand(locator))
}

func findCommand(request *consoleio.Request, response *consoleio.Response, locator *command.CommandLocator) command.Command {
	dispatcher := command.NewDispatcher(locator)
	return dispatcher.Dispatch(request, response)
}

func logResponseErrors(context Context, response *consoleio.Response) {
	if response.HasErrors() {
		context.LogMethod(response.Errors)
	}
}

func outputErrorMessages(response *consoleio.Response, context Context) {
	if response.Verbosity && len(response.Messages) > 0 {
		for _, msg := range response.Messages {
			context.OutputMethod(msg)
		}
	}
}

func Execute(context Context) {
	request, response := createIo(context.Arguments)
	commandLocator := context.CommandLocator
	addHelpCommand(commandLocator)

	curCommand := findCommand(request, response, commandLocator)

	if validateRequestInCommand(request, response, curCommand) == nil {
		curCommand.Execute(request, response)
	}

	logResponseErrors(context, response)
	outputErrorMessages(response, context)
}