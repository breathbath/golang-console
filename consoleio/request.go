package consoleio

import (
	"strings"
)

type Request struct {
	CommandName string
	Parameters  map[string]string
	Options     map[string]string
}

func NewRequestFromArgs(Args []string) *Request {
	parameters := make(map[string]string)
	options := make(map[string]string)

	commandName := ""
	var foundOption string
	for k, arg := range Args {
		if k == 0 {
			continue;
		}
		if (strings.HasPrefix(arg, "-")) {
			foundOption = strings.TrimLeft(arg, "-")
			options[foundOption] = foundOption
		} else {
			if commandName == "" {
				commandName = arg
			} else {
				argKey, argVal := extractCommandParam(arg)
				parameters[argKey] = argVal
			}
		}
	}
	return &Request{commandName, parameters, options}
}

func extractCommandParam(inptStr string) (string, string) {
	if strings.Contains(inptStr, "=") {
		spltString := strings.Split(inptStr, "=")
		return spltString[0], spltString[1]
	} else {
		return inptStr, inptStr
	}
}

func (this *Request) GetOptionString(name string) (string, bool) {
	val, ok := this.Options[name]
	return val, ok
}

func (this *Request) GetParameterString(name string) (string, bool) {
	val, ok :=  this.Parameters[name]
	return val, ok
}

func (this *Request) CommandNameEmpty() bool {
	return this.CommandName == ""
}