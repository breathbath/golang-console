package consoleio

type OptionsCollection [] Option

func (options *OptionsCollection) GetOptionsNames() []string {
	var optionsNames []string
	for _, option := range *options {
		optionsNames = append(optionsNames, "-" + option.Name)
	}

	return optionsNames
}

func (options *OptionsCollection) HasOption(optionName string) bool {
	for _, currOption := range *options {
		if currOption.Name == optionName {
			return true
		}
	}
	return false
}

func NewGeneralOptions() OptionsCollection {
	return OptionsCollection{
		Option{Name: "h", Description: "Display help message"},
		Option{Name: "v", Description: "Verbose output"},
		Option{Name: "n", Description: "No input questions"},
	}
}