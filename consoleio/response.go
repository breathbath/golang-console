package consoleio
import (
	"errors"
)

type Response struct {
	Messages []string
	Errors [] error
	Verbosity bool
}

func NewResponse() *Response {
	return &Response{Messages:[]string{}, Errors: []error{}, Verbosity:true}
}

func (this *Response) AddError(err error) {
	this.Errors = append(this.Errors, err)
}

func (this *Response) AddErrorMessage(message string)  {
	this.Errors  = append(this.Errors, errors.New(message))
}

func (this *Response) AddMessage(msg string) {
	this.Messages = append(this.Messages, msg)
}

func (this *Response) HasErrors() bool {
	return len(this.Errors) != 0
}

func (this *Response) SetVerbose(on bool){
	this.Verbosity = on
}