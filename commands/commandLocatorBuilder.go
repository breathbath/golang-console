package commands

import "bitbucket.org/breathbath/go-console/command"

func BuildCommandsLocator() *command.CommandLocator {
	locator := command.NewCommandLocator()
	locator.AddCommand(NewPwdCommand())
	locator.AddCommand(NewDrawPyramidCommand())
	locator.AddCommand(NewFibonacci())

	return locator
}