package commands

import (
	"bitbucket.org/breathbath/go-console/command"
	"bitbucket.org/breathbath/go-console/consoleio"
	"os/exec"
)

const PWD_NAME  = "pwd"

type Pwd struct{
	command.Command
}

func NewPwdCommand() command.Command {
	return &Pwd{}
}

func (pwd Pwd) Execute(request *consoleio.Request, response *consoleio.Response) {
	out, err := exec.Command("sh", "-c", "pwd").Output()
	if err != nil {
		response.AddError(err)
	} else {
		response.AddMessage(string(out))
	}
}

func (pwd Pwd) GetPackageName() string {
	return "system"
}

func (pwd Pwd) GetName() string {
	return PWD_NAME
}

func (pwd Pwd) GetDescription() string {
	return "Shows current working directory"
}

func (pwd Pwd) Validate(request *consoleio.Request) error {
	return nil
}