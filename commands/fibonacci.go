package commands

import (
	"bitbucket.org/breathbath/go-console/command"
	"bitbucket.org/breathbath/go-console/consoleio"
	"bitbucket.org/breathbath/go-console/validators"
	"strconv"
	"fmt"
)

const FIB_NAME = "fibonacci"

type Fibonacci struct {
	command.ParametrisedCommand
}

func NewFibonacci() command.Command {
	command := &Fibonacci{}

	countParameter := consoleio.Parameter{ParamName:"count", IsRequired:true, Description:"The count of fibonacci numbers"}
	command.AddParameter(countParameter)
	command.AddParameterValidator(&countParameter, validators.UnsignedIntegerValidator{})
	command.AddOption(consoleio.Option{Name:"sc", Description:"Show numbers count"})

	return command
}

func (f Fibonacci) Execute(request *consoleio.Request, response *consoleio.Response) {
	countParam, _ := request.GetParameterString("count")
	count, _ := strconv.Atoi(countParam)

	fibNumbers := []float64{0, 1}

	prev, next := 0.0, 1.0

	for i := 0; i < count; i++ {
		prev, next = next, prev + next
		fibNumbers = append(fibNumbers, next)
	}

	fmt.Println(fibNumbers)

	if _, ok := request.GetOptionString("sc"); ok {
		fmt.Println(len(fibNumbers))
	}
}

func (f Fibonacci) GetPackageName() string {
	return "math"
}

func (f Fibonacci) GetName() string {
	return FIB_NAME
}

func (f Fibonacci) GetDescription() string {
	return "Shows fibonacci numbers"
}