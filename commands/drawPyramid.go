package commands

import (
	"bitbucket.org/breathbath/go-console/command"
	"bitbucket.org/breathbath/go-console/consoleio"
	"bitbucket.org/breathbath/go-console/validators"
	"strconv"
	"fmt"
)
type DrawPyramid struct {
	command.ParametrisedCommand
	DrawingMatrix [][]bool
}

func NewDrawPyramidCommand() command.Command {
	currCommand := &DrawPyramid{}

	signParameter := consoleio.Parameter{ParamName:"sign", IsRequired:false, DefaultValue:".", Description:"Sign used to draw the pyramid"}
	currCommand.AddParameter(signParameter)

	countParameter := consoleio.Parameter{ParamName:"count", IsRequired:true, Description:"The count of fibonacci numbers"}
	currCommand.AddParameter(countParameter)
	currCommand.AddParameterValidator(&countParameter, validators.UnsignedIntegerValidator{})

	return currCommand
}

func (this DrawPyramid) Execute(request *consoleio.Request, response *consoleio.Response) {
	countParam := this.GetParam("count", request)
	rowsCount, _ := strconv.Atoi(countParam.Value)
	signParam := this.GetParam("sign", request)
	columnsCount := rowsCount * 2 - 1

	var matrix [][]bool
	var row [] bool
	pointsCount := 1
	middleIndex := rowsCount - 1
	upIndex, downIndex := middleIndex, middleIndex
	value := false
	for i := 0; i < rowsCount; i++ {
		row = []bool{}
		downIndex = middleIndex - i
		upIndex = middleIndex + i
		for y := 0; y < columnsCount; y++ {
			if y >= downIndex && y <= upIndex {
				value = true
			} else {
				value = false
			}
			row = append(row, value)
		}
		matrix = append(matrix, row)
		pointsCount = pointsCount + 2
	}

	this.PrintMatrix(matrix, signParam.Value)
}

func (this DrawPyramid) PrintMatrix(matrix [][]bool, signParam string) {
	var symb string
	for x, v1 := range matrix {
		for y, _ := range v1 {
			if matrix[x][y] {
				symb = signParam
			} else {
				symb = " "
			}
			fmt.Print(symb)
		}
		fmt.Print("\n")
	}
}

func (this DrawPyramid) GetPackageName() string {
	return "graph"
}

func (this DrawPyramid) GetName() string {
	return "drawpyramid"
}

func (this DrawPyramid) GetDescription() string {
	return "Draws pyramids"
}

