package command

import "bitbucket.org/breathbath/go-console/consoleio"

type NullCommand struct{}

func (this *NullCommand) Validate(request *consoleio.Request) error {
	return nil
}