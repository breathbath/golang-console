package command

import (
	"bitbucket.org/breathbath/go-console/consoleio"
)

type Command interface {
	Execute(request *consoleio.Request, response *consoleio.Response)
	GetName() string
	GetPackageName() string
	GetDescription() string
	Validate(request *consoleio.Request) error
}