package command

import "bitbucket.org/breathbath/go-console/consoleio"

type Dispatcher struct {
	locator *CommandLocator
}

func NewDispatcher(locator *CommandLocator) *Dispatcher {
	return &Dispatcher{locator: locator}
}

func (this *Dispatcher) Dispatch(request *consoleio.Request, response *consoleio.Response) Command {
	defaultCommand := func() Command {
		i, _ := this.locator.GetCommandByName("help")
		return i
	}
	_, hasHelpParam := request.GetOptionString("h")
	if hasHelpParam {
		return defaultCommand()
	}

	return this.fetchCommand(request.CommandName, defaultCommand)
}

func (this *Dispatcher) fetchCommand(name string, defaultCommand func() Command) Command {
	command, ok := this.locator.GetCommandByName(name)
	if !ok {
		return defaultCommand()
	}
	return command
}