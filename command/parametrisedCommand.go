package command

import (
	"bitbucket.org/breathbath/go-console/consoleio"
	"bitbucket.org/breathbath/go-console/validators"
	"errors"
	"strings"
)

type ParametrisedCommand struct {
	Params     []consoleio.Parameter
	Options consoleio.OptionsCollection
	Validators map[string][]validators.ParameterValidator
}

func (this *ParametrisedCommand) GetParam(paramName string, request *consoleio.Request) (consoleio.Parameter) {
	registeredParam := this.getRegisteredParamByName(paramName)
	if &registeredParam == nil {
		return registeredParam
	}

	paramValue, ok := request.GetParameterString(paramName)
	if ok {
		registeredParam.Value = paramValue
	} else {
		registeredParam.Value = registeredParam.DefaultValue
	}

	return registeredParam
}

func (this *ParametrisedCommand) HasOption(name string, request *consoleio.Request) bool {
	_, ok := request.GetOptionString(name)
	return ok
}

func (this *ParametrisedCommand) getRegisteredParamByName(name string) consoleio.Parameter {
	for _, registeredParam := range this.Params {
		if registeredParam.ParamName == name {
			return registeredParam
		}
	}
	return consoleio.Parameter{}
}

func (this *ParametrisedCommand) AddParameter(param consoleio.Parameter) {
	if this.Params == nil {
		this.Params = []consoleio.Parameter{}
	}
	this.Params = append(this.Params, param)
}

func (this *ParametrisedCommand) AddOption(option consoleio.Option) {
	if this.Options == nil {
		this.Options = []consoleio.Option{}
	}
	this.Options = append(this.Options, option)
}

func (this *ParametrisedCommand) AddParameterValidator(parameter *consoleio.Parameter, validator validators.ParameterValidator) {
	if this.Validators == nil {
		this.Validators = make (map[string][]validators.ParameterValidator)
	}
	currSlice := this.Validators[parameter.ParamName]
	currSlice = append(currSlice, validator)

	this.Validators[parameter.ParamName] = currSlice
}

func (this *ParametrisedCommand) hasValidators() bool {
	return this.Validators != nil
}

func (this *ParametrisedCommand) validateParameters(request *consoleio.Request) error {
	missingRequiredParams := []string{}
	for _, param := range this.Params {
		paramValue, found := request.GetParameterString(param.ParamName)

		if !found && param.IsRequired {
			missingRequiredParams = append(missingRequiredParams, param.ParamName)
		}

		if this.hasValidators() {
			paramValidators, ok := this.Validators[param.ParamName]
			if ok {
				for _, paramValidator := range paramValidators {
					err := paramValidator.Validate(paramValue, param, *request)
					if err != nil {
						return err
					}
				}
			}
		}
	}
	if len(missingRequiredParams) > 0 {
		return errors.New("Missing required command param(s): " + strings.Join(missingRequiredParams, ","))
	}
	return nil
}

func (this ParametrisedCommand) Validate(request *consoleio.Request) error {
	if len(this.Params) > 0 {
		return this.validateParameters(request)
	}
	return nil
}

func (this *ParametrisedCommand) GetParams() []consoleio.Parameter {
	if this.Params == nil {
		return []consoleio.Parameter{}
	}
	return this.Params
}

func (this *ParametrisedCommand) GetOptions() consoleio.OptionsCollection {
	if this.Options == nil {
		return consoleio.OptionsCollection{}
	}
	return this.Options
}