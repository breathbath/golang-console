package command

import (
	"bitbucket.org/breathbath/go-console/consoleio"
	"bitbucket.org/breathbath/go-console/validators"
)

type CommandWithParams interface {
	GetParam(paramName string, request *consoleio.Request) (consoleio.Parameter)
	AddParameter(param consoleio.Parameter)
	AddParameterValidator(parameter *consoleio.Parameter, validator validators.ParameterValidator)
	GetParams() []consoleio.Parameter
	HasOption(name string, request *consoleio.Request) bool
	AddOption(option consoleio.Option)
	GetOptions() consoleio.OptionsCollection
}